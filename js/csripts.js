// ----------- №1 ------------

const str = 'aaa@bbb@ccc'
console.log(str.replace(/@/g, '!'));


// ----------- №2 ------------


const data = '2025-12-31';
const newData = data.replace(/(2025)(-)(12)(-)(31)/, '$5/$3/$1');
console.log(newData);



// ----------- №3 ------------
const str3 = 'Я учу javascript!';

console.log(str3.substring(2, 5));
console.log(str3.substring(6, 16));

console.log(str3.substr(2, 3));
console.log(str3.substr(6, 10));

console.log(str3.slice(2, 5));
console.log(str3.slice(6, 16));


// ----------- №4 ------------

const arr = [4, 2, 5, 19, 13, 0, 10];
let sum = 0;


for(let i = 0; i < arr.length; i++) {
    sum += Math.pow(arr[i], 3);
}

const sqrtSum = Math.sqrt(sum);
console.log(sqrtSum);


// ----------- №5 ------------


const a = 3;
const b = 5;

const c = Math.abs(a - b);
console.log(c);


// ----------- №6 ------------

let dataNow = new Date();

const addZero = (number) => number < 10 ? '0' + number : number;

console.log(`${dataNow.getHours()}:${dataNow.getMinutes()}:${dataNow.getSeconds()} ${addZero(dataNow.getDate())}.${addZero(dataNow.getMonth()+1)}.${dataNow.getFullYear()}`)


// ----------- №7 ------------


const str7 ='aa aba abba abbba abca abea';

console.log(str7.match(/ab+a/g));


// ----------- №8 ------------

const telNumber = '+375(33) 333-33-33'

// const parTel = (tel) => {
//     const regExp = /^(\+375\(\d{2}\)\s)(\d{3}-\d{2}-\d{2})$/gi.test(tel);
//     const result = tel.test(regExp);
//     if (result != null) {
//         return true
//     } else {
//         return false
//     }
//     return regExp
// }

// console.log(parTel(telNumber));

const parTel = (tel) =>  /^(\+375\(\d{2}\)\s)(\d{3}-\d{2}-\d{2})$/gi.test(tel);
    
console.log(parTel(telNumber));


// ----------- №9 ------------

const email = 'Test.test-TEST_js@gmail.com';


const parEmail = (email) => /^([a-zA-Z\d]{1,}[\w.-]{2,}[a-zA-Z\d]{1,})(@[a-z]{2,11}\.[a-z]{2,11})$/gi.test(email);

console.log(parEmail(email));


// ----------- №10 ------------


const parUrl = (url) => {
    const regExp = /^(https?:\/{2}[\w\.-]*)(\/[\w\/-]*)?(\?[\w=&-]*)?(#.*)?$/
    const parUrlArr = url.match(regExp);
    console.log(parUrlArr);
}

parUrl('https://tech.onliner.by/2018/04/26/smart-do-200/?utm_source=main_tile&utm_medium=smartdo200#zag3')
